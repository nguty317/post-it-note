import javafx.application.Application;
import javafx.stage.Stage;

public class PostItNote extends Application {
	private PostItNoteStage mainWindow;	
	
	public PostItNote() {	
		mainWindow = new PostItNoteStage(250, 250, 0, 0);
	}
	
	@Override
	public void start(Stage stage) {
	}
	
	public static void main(String[] args) {
		System.out.println("Starting Post-It Note application...");
		System.out.println("Author: Thu Hien Nguyen");
		
		launch(args);
	}

	
}

