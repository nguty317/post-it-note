import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DialogPane;
import javafx.scene.control.IndexRange;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class PostItNoteStage {
	static private BorderPane content;
	static private BorderPane buttonArea;
	static private Button newPostItNote;
	static private Button deletePostItNote;
	static private TextArea textArea;
	static private Font buttonFont;
	static private ContextMenu rightClickMenu;
	static private MenuItem cut;
	static private MenuItem copy;
	static private MenuItem paste;
	static private MenuItem about;
	static private MenuItem exit;
	static private double xOffset;
	static private double yOffset;
	static private Button resize;
	static private BorderPane resizeArea;

	public PostItNoteStage(double sizeX, double sizeY, double positionX, double positionY) {

		// Implement the stage.
		Stage stage = new Stage();
		stage.setX(positionX);
		stage.setY(positionY);
		stage.initStyle(StageStyle.UNDECORATED);

		// Implement the class variables.
		content = new BorderPane();
		buttonArea = new BorderPane();
		newPostItNote = new Button("+");
		deletePostItNote = new Button("x");
		textArea = new TextArea();
		resizeArea = new BorderPane();
		resize = new Button("Resize");

		// Set up the note's layout.
		buttonArea.setLeft(newPostItNote);
		buttonArea.setRight(deletePostItNote);
		resizeArea.setRight(resize);
		content.setTop(buttonArea);
		content.setCenter(textArea);
		content.setBottom(resizeArea);
		
		// Implement the scene.
		Scene scene = new Scene(content, sizeX, sizeY);
		stage.setScene(scene);
		stage.show();

		// Style all components.
		content.setEffect(new DropShadow());
		content.setStyle("-fx-background-color: rgb(50,50,50)");
		buttonArea.setStyle("-fx-background-color: rgb(255,215,0)");
		newPostItNote.setStyle("-fx-border-color: transparent; -fx-background-color: transparent; "
				+ "-fx-border-width: 0; -fx-background-radius: 0");
		deletePostItNote.setStyle("-fx-border-color: transparent; -fx-background-color: transparent; "
				+ "-fx-border-width: 0; -fx-background-radius: 0");
		textArea.setStyle(
				"-fx-background-color: rgb(50,50,50); -fx-background-radius: 0; -fx-border-radius: 0; -fx-font-family: Calibri; "
						+ "-fx-text-fill: rgb(255,255,255); -fx-highlight-fill: rgb(218,165,32); -fx-font-size: 18;");
		buttonFont = Font.font("Arial", FontWeight.BOLD, 20);
		newPostItNote.setFont(buttonFont);
		newPostItNote.setTextFill(Color.GREY);
		deletePostItNote.setFont(buttonFont);
		deletePostItNote.setTextFill(Color.GREY);
		Region region = (Region) textArea.lookup(".content");
		region.setStyle("-fx-background-color: rgb(50,50,50)");
		resize.setTextFill(Color.GREY);
		resize.setStyle(
				"-fx-border-color: transparent; -fx-background-color: rgb(40,40,40); -fx-text-color: rgb(255,255,255); "
						+ "-fx-border-width: 0; -fx-background-radius: 3");

		// Implement the add new Note button.
		EventHandler<ActionEvent> newButton = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				// Variables for new note's position.
				double newX;
				double newY;

				// Variable for computer's screen.
				Rectangle2D screen = Screen.getPrimary().getVisualBounds();

				// If reach the screen's border, 
				//add a new note at the beginning of a new row (20px below the old row).
				if ((stage.getX() + stage.getWidth() * 2) > screen.getWidth()) {
					newX = 0;
					newY = stage.getY() + stage.getHeight() + 20;
					
					// Else, add a new note (20px to the right of previous note).
				} else {
					newX = stage.getX() + stage.getWidth() + 20;
					newY = stage.getY();
				}
				new PostItNoteStage(sizeX, sizeY, newX, newY);

			}
		};

		newPostItNote.setOnAction(newButton);

		// Implement the close Note button.
		EventHandler<ActionEvent> deleteButton = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				stage.close();
			}
		};

		deletePostItNote.setOnAction(deleteButton);

		// Create the menu and add menu items.
		rightClickMenu = new ContextMenu();

		cut = new MenuItem("Cut");
		rightClickMenu.getItems().add(cut);

		copy = new MenuItem("Copy");
		rightClickMenu.getItems().add(copy);

		paste = new MenuItem("Paste");
		rightClickMenu.getItems().add(paste);

		about = new MenuItem("About");
		rightClickMenu.getItems().add(about);

		exit = new MenuItem("Exit");
		rightClickMenu.getItems().add(exit);

		// Show the menu on mouse clicking.
		textArea.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);

		EventHandler<MouseEvent> rightClick = new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				if (e.getButton() == MouseButton.SECONDARY) {
					rightClickMenu.show(content, e.getScreenX(), e.getScreenY());
				}
			}
		};

		content.setOnMouseClicked(rightClick);
		textArea.setOnMouseClicked(rightClick);

		// Implement the functions of menu items.
		Clipboard systemClipboard = Clipboard.getSystemClipboard();

		// Implement cut function.
		EventHandler<ActionEvent> cutAction = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				// Store selected text in the clipboard.
				String cutText = textArea.getSelectedText();
				ClipboardContent clipboardContent = new ClipboardContent();
				clipboardContent.putString(cutText);
				systemClipboard.setContent(clipboardContent);

				// Range of the selected text.
				IndexRange range = textArea.getSelection();
				// Original text.
				String originalText = textArea.getText();

				// First part of the altered text is from the beginning to the start of selected text.
				String firstPart = originalText.substring(0, range.getStart());
				// Second part of the altered text is from end of selected text to the end of the note.
				String lastPart = originalText.substring(range.getEnd(), originalText.length());
				// Put altered text into the note.
				textArea.setText(firstPart + lastPart);
				// Put the mouse at the position of the text that has been cut.
				textArea.positionCaret(range.getStart());
			}
		};

		cut.setOnAction(cutAction);

		// Implement copy function.
		EventHandler<ActionEvent> copyAction = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				// Store selected text in the clipboard.
				String copyText = textArea.getSelectedText();
				ClipboardContent clipboardContent = new ClipboardContent();
				clipboardContent.putString(copyText);
				systemClipboard.setContent(clipboardContent);
			}
		};

		copy.setOnAction(copyAction);

		// Implement paste function.
		EventHandler<ActionEvent> pasteAction = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				// Check if there is content in the clipboard to paste.
				if (systemClipboard.hasString()) {
					// Get string to paste.
					String pasteText = systemClipboard.getString();
					// Get the range of the text that are selected.
					IndexRange range = textArea.getSelection();
					// Original text.
					String originalText = textArea.getText();

					// Alter the text after pasting.
					int endPos = 0;
					String updatedText = "";
					String firstPart = originalText.substring(0, range.getStart());
					String lastPart = originalText.substring(range.getEnd(), originalText.length());
					updatedText = firstPart + pasteText + lastPart;

					// Set the mouse position.
					if (range.getStart() == range.getEnd()) {
						endPos = range.getEnd() + pasteText.length();
					} else {
						endPos = range.getStart() + pasteText.length();
					}

					textArea.setText(updatedText);
					textArea.positionCaret(endPos);
				}
			}
		};

		paste.setOnAction(pasteAction);

		// Implement exit function.
		EventHandler<ActionEvent> exitAction = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				stage.close();
			}
		};

		exit.setOnAction(exitAction);

		// Implement about window.
		EventHandler<ActionEvent> aboutAction = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				// Create new Alert.
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Post-It Note");
				alert.setHeaderText("Post-It Note Application");

				// Create grid pane in the alert
				GridPane grid = new GridPane();
				grid.setPadding(new Insets(15, 15, 15, 15));
				grid.setHgap(15);

				// Insert image to grid pane.
				Image image = new Image("hazel.jpg");
				
				ImageView imageView = new ImageView(image);
				imageView.setFitWidth(112);
				imageView.setFitHeight(133);

				// Insert text to grid pane.
				Text text = new Text(
						"Digital Post-It Note using JavaFX\nAuthor: Hazel (Hien) Nguyen\nStudent email ID: nguty317\nCopy right (c) 2020");

				grid.add(imageView, 0, 0);
				grid.add(text, 1, 0);

				// Insert grid pane to the alert.
				alert.getDialogPane().setContent(grid);
				alert.showAndWait();
			}
		};

		about.setOnAction(aboutAction);

		// Implement window moving function.
		buttonArea.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				// Get the offset between the stage position and mouse position.
				xOffset = stage.getX() - e.getScreenX();
				yOffset = stage.getY() - e.getScreenY();
			}
		});

		buttonArea.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				// Add the offset to stage's size.
				stage.setX(e.getScreenX() + xOffset);
				stage.setY(e.getScreenY() + yOffset);
			}
		});

		// Implement resize function.
		resize.setOnMouseMoved(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				// Set mouse cursor to resize cursor when it moved to the resize button.
				if (e.getEventType() == MouseEvent.MOUSE_MOVED) {
					resize.setCursor(Cursor.NW_RESIZE);
				}
			}
		});

		resize.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				// Get the offset between stage's sizes and mouse position.  
				xOffset = stage.getWidth() - e.getScreenX();
				yOffset = stage.getHeight() - e.getScreenY();
			}
		});

		resize.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				// Change stage's size if the new size is bigger than the minimum size.
				if (e.getScreenX() + xOffset >= sizeX && e.getScreenY() + yOffset >= sizeY) {
					stage.setWidth(e.getScreenX() + xOffset);
					stage.setHeight(e.getScreenY() + yOffset);
				}
			}
		});

	}

	public static void main(String[] args) {

	}
}
